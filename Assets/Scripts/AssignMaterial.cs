﻿using UnityEngine;
using System.Collections;

public class AssignMaterial : MonoBehaviour {

	public Material house;
	// Use this for initialization
	void Start ()
	{
		WithForLoop();
	}
	void WithForLoop()
	{
			int children = transform.childCount;
			for (int i = 0; i < children; ++i)
				{
					var game = transform.GetChild(i);
					game.GetComponent<Renderer>().material = house;
					var obj = game.GetComponent<Renderer>().material;
					Debug.Log("For loop: " + obj);
				}
	}

}
